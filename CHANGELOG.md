## Вышла версия 2.0.0.224  
 
### Chapter Generator  

#### Space Marine - Space Marine  
- правка багов вещей  и спрайтов 

#### Space Marine - Primaris  
- добавлены схемы раскраски Black Templars, Blood Angels, Blood Ravens, Crimson Fists, Dark Angels,  Imperial Fists, Iron Hands, Raven Guard, Salamanders, Space Wolves, Ultramarines, White Scars

#### Space Marine - Terminator  
- правка багов вещей  и спрайтов 
 
#### Chaos Space Marine - Space Marine  
- добавлен модуль Дети Императора  
  
### Chapter Constructor  
 - поправлены баги интерфейса
 
#### Space Marine - Space Marine  
- поправлены баги знаков пре-ереси

#### Space Marine - Terminator
- добавлен модуль Space Marine - Terminator

### SDK
- правка багов

## Вышла версия 2.0.0.223  
 
### Chapter Generator  
  
#### Space Marine - Terminator  
- добавлена броня Tartaros  
- изменено положение тяжелого оружия  
- добавлен комби-болтер Tigrus  
- добавлен пилокулак для брони Tartaros  
- добавлен пилокулак для брони Catafractii  
- добавлен силовой кулак для брони Tartaros  
- добавлен молниевый коготь для брони Tartaros  
- добавлена силовая булава  
- добавлена плазменная пушка  
  
#### Space Marine - Space Marine  
- правка багов вещей  
  
#### Chaos Space Marine - Space Marine  
- добавлен знак Детей Императора  
  
### Chapter Constructor  
#### Space Marine - Space Marine  
- добавлены знаки пре-ереси  
  
### SDK
- переход на .net framework 4.7.2  
- правка багов локализации


## Версия 2.0.0.222

### Chapter Generator

#### Space Marine - Primaris
- добавлена броня Phobos
- добавлена броня Reiver
- добавлен болт-карабин
- добавлен болт-карабин Marksman
- добавлен болт-карабин Instigator
- добавлен болт-карабин Occulus
- добавлен крюкомет
- изменена вероятность появления маски

#### Space Marine - Space Marine
- добавлен абордажный щит
- изменена вероятность появления маски

#### Space Marine - Scout
- добавлены символы на наплечники

**Chaos Space Marine - Space Marine**
- изменена вероятность появления маски

### Chapter Constructor
 - добавлено сохранение состояния при выключении

#### Space Marine - Space Marine
- добавлен абордажный щит

### SDK
- добавлена поддержка английского языка
- правка багов
- оптимизация